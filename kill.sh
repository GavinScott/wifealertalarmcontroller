#!/usr/bin/env bash

PID=$(ps -aux | grep -v "grep" | grep wife-alert-alarm-controller.py | awk '{print $2}')

echo "Killing process $PID"

kill -9 $PID

echo "Done"