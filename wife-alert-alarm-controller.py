import boto3
import time
import RPi.GPIO as GPIO
import json
import logging
import datetime
import os

ALARM_PIN = 11


def get_path_in_cwd(fname):
    return '/'.join(os.path.abspath(__file__).split('/')[:-1]) + f'/{fname}'


def cleanup():
    logging.info('Cleaning up GPIO')
    GPIO.output(ALARM_PIN, GPIO.LOW)


def run_startup_sequence(ssm):
    # tests AWS connection, flashes the light once if it worked
    logging.info('Running startup sequence')
    GPIO.output(ALARM_PIN, GPIO.LOW)
    ssm.get_parameter(
        Name='WifeAlertParam',
        WithDecryption=True)
    GPIO.output(ALARM_PIN, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(ALARM_PIN, GPIO.LOW)


logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
    datefmt='%y-%m-%d %H:%M:%S',
    filename=get_path_in_cwd('alarm-controller.log'),
    filemode='w')
logging.info('Time: ' + datetime.datetime.now().isoformat())

console = logging.StreamHandler()
console.setLevel(logging.INFO)
console.setFormatter(logging.Formatter(
    '%(name)-12s: %(levelname)-8s %(message)s'))
logging.getLogger().addHandler(console)

logging.info('Setting up GPIO')
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(ALARM_PIN, GPIO.OUT)
GPIO.output(ALARM_PIN, GPIO.LOW)

logging.info('Loading credentials from file')
with open(get_path_in_cwd('creds.json'), 'r') as f:
    creds = json.load(f)

logging.info('Setting up SSM client')
ssm = boto3.Session(
    aws_access_key_id=creds['aws_access_key_id'],
    aws_secret_access_key=creds['aws_secret_access_key'],
    region_name=creds['region']).client('ssm')

run_startup_sequence(ssm)

inAlarmState = False
logging.info('Beginning poll')
try:
    while True:
        param = ssm.get_parameter(
            Name='WifeAlertParam',
            WithDecryption=True)
        value = param['Parameter']['Value']
        if not inAlarmState and value == 'ALARM':
            logging.info('Entering alarm state')
            inAlarmState = True
            GPIO.output(ALARM_PIN, GPIO.HIGH)
        elif inAlarmState and value == 'OK':
            logging.info('Leaving alarm state')
            inAlarmState = False
            GPIO.output(ALARM_PIN, GPIO.LOW)
        time.sleep(15 if inAlarmState else 2)
except (KeyboardInterrupt, SystemExit):
    logging.info('Terminating')
    cleanup()
    exit(0)
except Exception as ex:
    logging.error('Fatal Error!', exec_info=True)
    cleanup()
    exit(-1)
