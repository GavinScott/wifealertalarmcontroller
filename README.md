# README

This is a script that controls the Wife Alert alarm (red flashing light). It polls the value of the SSM parameter and starts flashing the light . It should ideally be configured to run whenever your Pi boots up.

## Setup

1. This will not work until you've deployed the WifeAlert CloudFormation stack to an AWS account and have an access/secret key pair for the `WifeAlertPollingUser`.
2. Create a file in the same directory as `wife-alert-alarm-controller.py` called `creds.json`. It should look like this:

```json
{
  "aws_access_key_id": "YOUR_ACCESS_KEY",
  "aws_secret_access_key": "YOUR_SECRET_KEY",
  "region": "YOUR_AWS_REGION"
}
```

## Setting up to run on boot

DISCLAIMER: This doesn't work well consistently, often requiring either multiple restarts or SSHing in and starting the script manually. I'd recommend investigating a better way to achieve this.

There are multiple ways to do this; I did it by modifying my `/etc/rc.local` file, adding these lines:

```
rm /home/pi/wifealertbuttoncontroller/*.log
python3 /home/pi/wifealertbuttoncontroller/wife-alert-button-controller.py &
```

You'll need to modify the paths to point to wherever you have the repository stored, but the paths should be absolute or it may not work. As an unrelated tip, I'd also recommend adding these lines to the top of your `/etc/rc.local` file top make debugging any issues easier (it'll output the commands it runs and any output to `/tmp/rc.local.log`):

```
exec 2> /tmp/rc.local.log
exec 1>&2
set -x
```

## Stopping the service

If you want to kill the service you can find its process id by running `ps` and killing it, or run `kill.sh` which will do it for you. If it was started be the root user you will need to use `sudo`.
